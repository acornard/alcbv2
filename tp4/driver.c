#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/cdev.h>
#include <linux/vmalloc.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/time.h>

#define SLAVE_ADR_R 0x91
#define SLAVE_ADR_W 0x90

#define GPIO_SDA_LABEL "gpio_sda"
#define GPIO_SCL_LABEL "gpio_scl"

#define INPUT_DIR 0
#define OUTPUT_DIR 1

#define LOW  0
#define HIGH 1

#define TIMEOUT 10000

static int BUF_SIZE = 32;
static int SDA_GPIO = 26;
static int SCL_GPIO = 19;
module_param(BUF_SIZE, int, S_IRUGO);
module_param(SDA_GPIO, int, S_IRUGO);
module_param(SCL_GPIO, int, S_IRUGO);

/* Pin data */
unsigned int sda_direction;
unsigned int scl_direction;

/* Device structures */
static dev_t dev;
struct cdev *my_cdev;

/* Functions headers */
static ssize_t my_read(struct file *f, char *buf, size_t size, loff_t *offset);
static ssize_t my_write(struct file *f, const char *buf, size_t size, loff_t *offset);
static int my_open(struct inode *inode, struct file *file);
static int my_release(struct inode *inode, struct file *file);

/* Files operations */
static struct file_operations fops = {
    .owner = THIS_MODULE,
    .read = my_read,
    .write = my_write,
    .open = my_open,
    .release = my_release,
};


static int my_driver_init(void){
    if(alloc_chrdev_region(&dev, 0, 1, "DEVICE") == -1){
        printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
        return -EINVAL;
    }

    printk(KERN_ALERT "Init allocated (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));
    printk(KERN_ALERT "SDA SCL = (%d,%d)\n", SDA_GPIO, SCL_GPIO);

    my_cdev = cdev_alloc();
    my_cdev->ops = &fops;
    my_cdev->owner = THIS_MODULE;


    /* Lien entre opération et périphérique */
    if(cdev_add(my_cdev, dev, 1) < 0){
        printk(KERN_ALERT ">>> ERROR cdev_add\n");
        return -EINVAL;
    }

    /* Allocation et enregistrement des gpios */
    gpio_request_one(SDA_GPIO, GPIOF_OUT_INIT_HIGH, GPIO_SDA_LABEL);
    gpio_request_one(SCL_GPIO, GPIOF_OUT_INIT_HIGH, GPIO_SCL_LABEL);
    sda_direction = OUTPUT_DIR;
    scl_direction = OUTPUT_DIR;

    return 0;
}

static void driver_cleanup(void){
    unregister_chrdev_region(dev, 1);
    cdev_del(my_cdev);
    gpio_free(SDA_GPIO);
    gpio_free(SCL_GPIO);
    printk(KERN_ALERT "Driver cleanup\n");
}


int i2c_send_address(void){
    int timeout;
    uint8_t mask, ack;
    mask = 1<<7;
    timeout = TIMEOUT;
    // Send address
    while(mask){
        if(mask & SLAVE_ADR_R){
            gpio_set_value(SDA_GPIO, HIGH);
        }	else {
            gpio_set_value(SDA_GPIO, LOW);
        }
        udelay(5);
        gpio_set_value(SCL_GPIO, HIGH);
        udelay(5);
        gpio_set_value(SCL_GPIO, LOW);
        mask = mask >> 1;
    }
    udelay(5);
    // Send ack and check status
    gpio_set_value(SDA_GPIO, HIGH);
    gpio_set_value(SCL_GPIO, HIGH);

    gpio_direction_input(SCL_GPIO);
    scl_direction = INPUT_DIR;

    gpio_direction_input(SDA_GPIO);
    sda_direction = INPUT_DIR;

    ack = gpio_get_value(SCL_GPIO);
    while((ack == LOW) && (timeout>0)){
        ack = gpio_get_value(SCL_GPIO);
        timeout --;
    }
    if(timeout == 0){
        printk(KERN_ALERT "ERROR: Timeout SCL LOW i2c_send_address \n");
       	return -1;
    }

    timeout = TIMEOUT;
    ack = gpio_get_value(SDA_GPIO);
    while((ack != LOW) && (timeout>0)){
        ack = gpio_get_value(SDA_GPIO);
        timeout --;
    }
    if(timeout == 0){
        printk(KERN_ALERT "ERROR: Timeout ACK on i2c_send_address \n");
       	return -1;
    }
    return 0;
}

int i2c_request_read(void){
    int sda_status, scl_status, timeout, status, started;
    timeout = TIMEOUT;
    started = 0;

    // Open pin as input
    if(sda_direction != INPUT_DIR){
        gpio_direction_input(SDA_GPIO);
        sda_direction = INPUT_DIR;
    }
    if(scl_direction != INPUT_DIR){
        gpio_direction_input(SCL_GPIO);
        scl_direction = INPUT_DIR;
    }

    while(!started && (timeout>0)){
        sda_status = gpio_get_value(SDA_GPIO);
        scl_status = gpio_get_value(SCL_GPIO);
        if(scl_status && sda_status){ // If bus is unused
            // Set SDA as output with low state
            gpio_direction_output(SDA_GPIO, LOW);
            sda_direction = OUTPUT_DIR;
            udelay(5);
            // Set SCL as output with high state
            gpio_direction_output(SCL_GPIO, LOW);
            scl_direction = OUTPUT_DIR;
            started = !started;
        }
        timeout --;
    }

    if(!started){
        printk(KERN_ALERT "ERROR: Timeout on i2c_request_read\n");
        //return -1;
    }

    status = i2c_send_address();
    if(status<0){
        printk(KERN_ALERT "ERROR: Fail to send address on i2c_request_read\n");
        return -1;
    }
    return 0;
}

int i2c_read_byte(uint8_t *byte){
    int offset, timeout;

    // Set sda, scl as input
    if(sda_direction != INPUT_DIR){
        gpio_direction_input(SDA_GPIO);
        sda_direction = INPUT_DIR;
    }
    if(scl_direction != INPUT_DIR){
        gpio_direction_input(SCL_GPIO);
        scl_direction = INPUT_DIR;
    }

    offset = 7;
    while(offset >= 0){
        timeout = TIMEOUT;
        while((gpio_get_value(SCL_GPIO) == LOW) && (timeout>0)){
            // Wait SCL high
            timeout --;
        }
        if(timeout == 0){
            printk(KERN_ALERT "ERROR: Wait SCL high timeout in i2c_read_byte \n");
            return -1;
        }
        *byte = *byte | (gpio_get_value(SDA_GPIO) << (offset));
        offset --;

        timeout = TIMEOUT;
        while((gpio_get_value(SCL_GPIO) == LOW) && (timeout>0)){
            // Wait SCL low
            timeout --;
        }
        if(timeout == 0){
            printk(KERN_ALERT "ERROR: Wait SCL high timeout in i2c_read_byte \n");
            return -1;
        }
    }
    return 0;
}

static ssize_t my_read(struct file *f, char *buf, size_t size, loff_t *offset){
    int status, data_size;
    uint8_t byteH, byteL;
    int16_t data;
    char data_str[32];

    // SCL test --------------------------------
    //    gpio_direction_output(SCL_GPIO, LOW);
    //    while(1){
    //		udelay(5);
    //		gpio_set_value(SCL_GPIO, HIGH);
    //		udelay(5);
    //		gpio_set_value(SCL_GPIO, LOW);       
    //    }
    //------------------------------------------

    status = i2c_request_read();
    if(status < 0){
        printk(KERN_ALERT "ERROR: i2c_request_read \n");
        return 0;
    }
    status = i2c_read_byte(&byteH);
    if(status<0){
        printk(KERN_ALERT "ERROR: i2c_read_bytes byteH \n");
        return 0;
    }
    gpio_direction_output(SDA_GPIO, LOW); // Continue transmission
    gpio_direction_input(SDA_GPIO);

    status = i2c_read_byte(&byteL);
    if(status<0){
        printk(KERN_ALERT "ERROR: i2c_read_bytes byteL\n");
        return 0;
    }

    data = (byteH << 8) | byteL;
    if(status<0){
        printk(KERN_ALERT "ERROR: i2c_read_bytes \n");
        return 0;
    }
    data_size = sprintf(data_str, "Valeur lue: %d \n", data);
    if(copy_to_user(buf, data_str, data_size)==0){
        return data_size;
    } else {
        printk(KERN_ALERT "ERROR: Copy to user\n");
        return -EINVAL;
    }
}

static ssize_t my_write(struct file *f, const char *buf, size_t size, loff_t *offset){
    printk(KERN_ALERT "Write is disable on this device !\n");
    return size;
}


static int my_open(struct inode *inode, struct file *file){
    printk(KERN_ALERT "Open called !\n");
    return 0;
}

static int my_release(struct inode *inode, struct file *file){
    printk(KERN_ALERT "Release called !\n");
    return 0;
}

module_init(my_driver_init);
module_exit(driver_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("CORNARD; alexis.cornard@univ-tlse3.fr");
MODULE_DESCRIPTION("Driver for KY-05");
MODULE_VERSION("0.1");
