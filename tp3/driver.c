#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/cdev.h>
#include <linux/vmalloc.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/time.h>
#include <linux/jiffies.h>

#define GPIO_ECHO 27
#define GPIO_TRIGGER 17

#define GPIO_ECHO_LABEL "gpio_echo"
#define GPIO_TRIGGER_LABEL "gpio_trigger"

static int BUF_SIZE = 32;
module_param(BUF_SIZE, int, S_IRUGO);

/* Device structures */
static dev_t dev;
struct cdev *my_cdev;

/* Functions headers */
static ssize_t my_read(struct file *f, char *buf, size_t size, loff_t *offset);
static ssize_t my_write(struct file *f, const char *buf, size_t size, loff_t *offset);
static int my_open(struct inode *inode, struct file *file);
static int my_release(struct inode *inode, struct file *file);

/* Files operations */
static struct file_operations fops = {
  .owner = THIS_MODULE,
  .read = my_read,
  .write = my_write,
  .open = my_open,
  .release = my_release,
};


static int my_driver_init(void){
  if(alloc_chrdev_region(&dev, 0, 1, "DEVICE") == -1){
    printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
    return -EINVAL;
  }

  printk(KERN_ALERT "Init allocated (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));

  my_cdev = cdev_alloc();
  my_cdev->ops = &fops;
  my_cdev->owner = THIS_MODULE;


  /* Lien entre opération et périphérique */
  if(cdev_add(my_cdev, dev, 1) < 0){
    printk(KERN_ALERT ">>> ERROR cdev_add\n");
    return -EINVAL;
  }

	/* Allocation et enregistrement des gpios */
	gpio_request_one(GPIO_ECHO, GPIOF_IN, GPIO_ECHO_LABEL);
	gpio_request_one(GPIO_TRIGGER, GPIOF_OUT_INIT_LOW, GPIO_TRIGGER_LABEL);
  return 0;
}

static void driver_cleanup(void){
  unregister_chrdev_region(dev, 1);
  cdev_del(my_cdev);
	gpio_free(GPIO_ECHO);
	gpio_free(GPIO_TRIGGER);
  printk(KERN_ALERT "Driver cleanup\n");
}

static ssize_t my_read(struct file *f, char *buf, size_t size, loff_t *offset){
	unsigned long distance;
	struct timeval ts, te;
	time_t duree_us;
	char value[BUF_SIZE];
	int size_s;

	size_s = 0;
	/* Procedure pour lire une valeur via le capteur */
	gpio_set_value(GPIO_TRIGGER, 1);
	udelay(10);
	gpio_set_value(GPIO_TRIGGER, 0);

	do_gettimeofday(&ts);
	while(gpio_get_value(GPIO_ECHO)==0)
		do_gettimeofday(&ts);

	do_gettimeofday(&te);
	while(gpio_get_value(GPIO_ECHO)==1)
		do_gettimeofday(&te);

	duree_us = te.tv_usec - ts.tv_usec;
	distance = ((duree_us * 34300)/2);
	distance = distance / 1000000;

	if((distance < 2) || (distance>300)){
		size_s = sprintf(value, "Invalid distance\n");
	} else {
		size_s = sprintf(value, "Distance: %lu cm\n", distance);
	}
	if(copy_to_user(buf, value, size_s)==0){
		return size_s;
	} else {
		printk (KERN_ALERT "ERROR: Copy to user\n");
		return -EINVAL;
	}
}

static ssize_t my_write(struct file *f, const char *buf, size_t size, loff_t *offset){
  printk(KERN_ALERT "Write is disable on this device !\n");
	return size;
}


static int my_open(struct inode *inode, struct file *file){
  printk(KERN_ALERT "Open called !\n");
  return 0;
}

static int my_release(struct inode *inode, struct file *file){
  printk(KERN_ALERT "Release called !\n");
  return 0;
}

module_init(my_driver_init);
module_exit(driver_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("CORNARD; alexis.cornard@univ-tlse3.fr");
MODULE_DESCRIPTION("Driver for KY-05");
MODULE_VERSION("0.1");
