#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>


#define IOCTL_CREATE_SNAPSHOT_MAGIC 'C'
#define IOCTL_CREATE_SNAPSHOT _IO(IOCTL_CREATE_SNAPSHOT_MAGIC, 0)

#define IOCTL_SNAPSHOT_BEFORE_MAGIC 'B'
#define IOCTL_SNAPSHOT_BEFORE _IO(IOCTL_SNAPSHOT_BEFORE_MAGIC, 0)

#define IOCTL_SNAPSHOT_AFTER_MAGIC 'A'
#define IOCTL_SNAPSHOT_AFTER _IO(IOCTL_SNAPSHOT_AFTER_MAGIC, 0)



int main(int argc, char **argv){
	int fd;

	if(argc == 3){
		fd = open(argv[1], O_RDONLY);
		if(fd < 0){
			perror("open");
			printf("ERROR: Can't open %s", argv[1]);
			return(errno);
		}
		if(argv[2] == "create")
			ioctl(fd, IOCTL_CREATE_SNAPSHOT, argv[2]);
		if(argv[2] == "before")
			ioctl(fd, IOCTL_SNAPSHOT_BEFORE, argv[2]);
		if(argv[2] == "after")
			ioctl(fd, IOCTL_SNAPSHOT_AFTER, argv[2]);
		close(fd);
	}
	else{
		printf("Usage ./do_ioctl {device path} {password}\n");
		return 1;
	}
}
