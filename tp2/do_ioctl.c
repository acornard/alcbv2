#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>


#define IOCTL_ED_MAGIC 'k'
#define IOCTL_ED _IO(IOCTL_ED_MAGIC, 0)


int main(int argc, char **argv){
	int fd;

	if(argc == 3){
		printf("Call to iotl_ed \n");
		fd = open(argv[1], O_RDONLY);
		if(fd < 0){
			perror("open");
			printf("ERROR: Can't open %s", argv[1]);
			return(errno);
		}
		ioctl(fd, IOCTL_ED, argv[2]);
		close(fd);
	}
	else{
		printf("Usage ./do_ioctl {device path} {password}\n");
		return 1;
	}
}
