/* Includes */
#include <linux/list.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/blk_types.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/ioctl.h>

#define NB_PART 2
#define NB_SECT 1024
#define SECT_SIZE 512

#define IOCTL_ED_MAGIC 'k'
#define IOCTL_ED _IO(IOCTL_ED_MAGIC, 0)

#define MAX_KEY_SIZE 32

static void rb_release(struct gendisk *gendsk, fmode_t mode);
static int rb_open(struct block_device *blk_dev, fmode_t mode);
static void rb_release(struct gendisk *gendsk, fmode_t mode);
static void rb_request(struct request_queue *);
static int rb_transfert(struct request *req);
static void * device_data;
static int encrypt_decrypt_ioctl(struct block_device *blkdev, fmode_t mode, unsigned int cmd, unsigned long arg);
const char BLOCK_NAME[] = "myblk";

static struct rb_device
{
  unsigned int size;              // Device size
  spinlock_t lock;                // For exclusive access
  struct request_queue *rb_queue; // Our request queue
  struct gendisk *rb_disk;        // Kernel's internal representation
} rb_dev;


static struct block_device_operations rb_fops =
{
  .owner = THIS_MODULE,
  .open = &rb_open,
  .release = &rb_release,
	.ioctl = &encrypt_decrypt_ioctl,
};


static int sample_init(void)
{
  int status, major;

	device_data = kmalloc(NB_SECT*SECT_SIZE, GFP_KERNEL);
	if (device_data ==  NULL){
		printk(KERN_ALERT "ERROR: kmalloc smaple_init return NULL pointer \n");
	}
  status = register_blkdev(0, BLOCK_NAME);
  if(status < 0){
    printk(KERN_ALERT "ERROR: sample_init register_blkdev\n");
    return -EINVAL;
  }
  major = status;
  printk(KERN_ALERT "Init allocated (major, BLOCK_NAME)=(%d, %s)\n", status, BLOCK_NAME);

  // Create request_queue
  spin_lock_init(&rb_dev.lock);
  rb_dev.rb_queue = blk_init_queue(rb_request, &rb_dev.lock);
  if (rb_dev.rb_queue == NULL) {
    printk(KERN_ALERT "ERROR: sample_init init_queue\n");
    return -EINVAL;
  }

  // Alloc gendisk struct
  rb_dev.rb_disk = alloc_disk(NB_PART+1);
  if (rb_dev.rb_disk == NULL) {
    printk(KERN_ALERT "ERROR: sample_init alloc_disk\n");
    return -EINVAL;
  }

  rb_dev.rb_disk->major = major;
  rb_dev.rb_disk->first_minor = 0;
  rb_dev.rb_disk->minors = NB_PART+1;
  rb_dev.rb_disk->fops = &rb_fops;
  rb_dev.rb_disk->queue = rb_dev.rb_queue;
  strcpy(rb_dev.rb_disk->disk_name, BLOCK_NAME);

  // Give disk capacity
  rb_dev.size = NB_SECT;
  set_capacity(rb_dev.rb_disk, rb_dev.size);

	// Add disk on kernel data struct
  add_disk(rb_dev.rb_disk);
  return 0;
}


static void sample_cleanup(void){
  int major = rb_dev.rb_disk->major;
	del_gendisk(rb_dev.rb_disk);
  put_disk(rb_dev.rb_disk);
  blk_cleanup_queue(rb_dev.rb_queue);
  unregister_blkdev(major, BLOCK_NAME);
	kfree(device_data);
  printk(KERN_ALERT "DEBUG: End of cleanup\n");
  return;
}


static void rb_request(struct request_queue * rb_queue){
	int status;
	struct request *req;
	status = 0;
	while((req = blk_fetch_request(rb_queue))!=NULL){
		status = rb_transfert(req);
		if(status<0){
  		printk(KERN_ALERT "ERROR: rb_request rb_transfert\n");
			return;
		}
		__blk_end_request_all(req, status);
	}
}

static int rb_transfert(struct request *req){
	int rw, sec_cnt, nSectors, sector_offset;
	sector_t start_sec;
	struct req_iterator iter;
	void *buffer;
	struct bio_vec bv;

	rw = rq_data_dir(req);
	start_sec = blk_rq_pos(req);
	sec_cnt = blk_rq_sectors(req);
	sector_offset = 0;

	rq_for_each_segment(bv, req, iter){
		buffer = page_address(bv.bv_page) + bv.bv_offset;
		if((bv.bv_len % SECT_SIZE) != 0){
  		printk(KERN_ALERT "ERROR: rb_transfert bad buffer size\n");
			return -EIO;
		}
		nSectors = bv.bv_len / SECT_SIZE;
		if(rw == WRITE){
			memcpy(device_data+(start_sec+sector_offset)*SECT_SIZE, buffer, nSectors * SECT_SIZE);
		} else {
			memcpy(buffer, device_data+(start_sec+sector_offset)*SECT_SIZE ,nSectors * SECT_SIZE);
		}
		sector_offset += nSectors;
	}
	if(sector_offset != sec_cnt){
  	printk(KERN_ALERT "ERROR: bad sector offset sum nSectors = %d, sec_cnt = %d \n", sector_offset, sec_cnt);
		return -EIO;
	}
	return 0;
}

int get_password_size(char *pass){
	int pass_size = 0;
	while((pass_size < MAX_KEY_SIZE) && (pass[pass_size]!=0))
		pass_size ++;
	printk(KERN_ALERT "DEBUG: Password size = %d \n", pass_size);
	return pass_size;
}

int encrypt_decrypt_data(char *pass){
	int offset = 0;
	int pass_size = get_password_size(pass);

	while(offset < SECT_SIZE * NB_SECT){
		((char *)device_data)[offset] ^= pass[offset%pass_size];
		offset+=1;
	}
	return 0;
}

static int encrypt_decrypt_ioctl
(struct block_device *blkdev, fmode_t mode, unsigned int cmd, unsigned long arg)
{
	// Check command
	int status;
	char *pass;
	pass = (char *) arg;
	if (_IOC_TYPE(cmd) != IOCTL_ED_MAGIC) return -ENOTTY;
	printk(KERN_ALERT "DEBUG: On ioctl  \n ");
	status = encrypt_decrypt_data(pass);
	printk(KERN_ALERT "DEBUG: Data encrypted  \n ");
	if(status < 0){
		printk(KERN_ALERT "ERROR: Error encrypt_decrypt_data \n");
		return(-1);
	}
	return 0;
}


static int rb_open(struct block_device *blk_dev, fmode_t mode){
//TODO
return 0;
}


static void rb_release(struct gendisk *gendsk, fmode_t mode){
// TODO
return ;
}


module_exit(sample_cleanup);
module_init(sample_init);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexis Cornard");
MODULE_DESCRIPTION("First block device");
MODULE_SUPPORTED_DEVICE("MyDevice");

