/* Includes */
#include <linux/list.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/blk_types.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/ioctl.h>

#define NB_PART 2
#define NB_SECT 1024
#define SECT_SIZE 512

#define IOCTL_CREATE_SNAPSHOT_MAGIC 'C'
#define IOCTL_CREATE_SNAPSHOT _IO(IOCTL_CREATE_SNAPSHOT_MAGIC, 0)

#define IOCTL_SNAPSHOT_BEFORE_MAGIC 'B'
#define IOCTL_SNAPSHOT_BEFORE _IO(IOCTL_SNAPSHOT_BEFORE_MAGIC, 0)

#define IOCTL_SNAPSHOT_AFTER_MAGIC 'A'
#define IOCTL_SNAPSHOT_AFTER _IO(IOCTL_SNAPSHOT_AFTER_MAGIC, 0)

#define MAX_KEY_SIZE 32
#define MAX_SNAPSHOT 10

static void rb_release(struct gendisk *gendsk, fmode_t mode);
static int rb_open(struct block_device *blk_dev, fmode_t mode);
static void rb_release(struct gendisk *gendsk, fmode_t mode);
static void rb_request(struct request_queue *);
static int rb_transfert(struct request *req);
static void * device_data;
static void ** snapshot_data;
static int ioctl_manager(struct block_device *blkdev, fmode_t mode, unsigned int cmd, unsigned long arg);
const char BLOCK_NAME[] = "myblk";
int nb_snapshot;
int current_snapshot;

static struct rb_device
{
  unsigned int size;              // Device size
  spinlock_t lock;                // For exclusive access
  struct request_queue *rb_queue; // Our request queue
  struct gendisk *rb_disk;        // Kernel's internal representation
} rb_dev;


static struct block_device_operations rb_fops =
{
  .owner = THIS_MODULE,
  .open = &rb_open,
  .release = &rb_release,
	.ioctl = &ioctl_manager,
};


static int sample_init(void)
{
  int status, major, i;

	device_data = kmalloc(NB_SECT*SECT_SIZE, GFP_KERNEL);
	if (device_data ==  NULL){
		printk(KERN_ALERT "ERROR: kmalloc smaple_init return NULL pointer \n");
	}

	snapshot_data = kmalloc(MAX_SNAPSHOT, GFP_KERNEL);
	if (device_data ==  NULL){
		printk(KERN_ALERT "ERROR: kmalloc snapshot_data return NULL pointer \n");
	}
	for(i=0; i<MAX_SNAPSHOT; i++){
		snapshot_data[i] = kmalloc(NB_SECT * SECT_SIZE, GFP_KERNEL);
		if (snapshot_data[i] ==  NULL){
			printk(KERN_ALERT "ERROR: kmalloc snapshot_data[i] return NULL pointer \n");
		}
	}

  status = register_blkdev(0, BLOCK_NAME);
  if(status < 0){
    printk(KERN_ALERT "ERROR: sample_init register_blkdev\n");
    return -EINVAL;
  }
	nb_snapshot = 0;
	current_snapshot = 0;
  major = status;
  printk(KERN_ALERT "Init allocated (major, BLOCK_NAME)=(%d, %s)\n", status, BLOCK_NAME);

  // Create request_queue
  spin_lock_init(&rb_dev.lock);
  rb_dev.rb_queue = blk_init_queue(rb_request, &rb_dev.lock);
  if (rb_dev.rb_queue == NULL) {
    printk(KERN_ALERT "ERROR: sample_init init_queue\n");
    return -EINVAL;
  }

  // Alloc gendisk struct
  rb_dev.rb_disk = alloc_disk(NB_PART+1);
  if (rb_dev.rb_disk == NULL) {
    printk(KERN_ALERT "ERROR: sample_init alloc_disk\n");
    return -EINVAL;
  }

  rb_dev.rb_disk->major = major;
  rb_dev.rb_disk->first_minor = 0;
  rb_dev.rb_disk->minors = NB_PART+1;
  rb_dev.rb_disk->fops = &rb_fops;
  rb_dev.rb_disk->queue = rb_dev.rb_queue;
  strcpy(rb_dev.rb_disk->disk_name, BLOCK_NAME);

  // Give disk capacity
  rb_dev.size = NB_SECT;
  set_capacity(rb_dev.rb_disk, rb_dev.size);

	// Add disk on kernel data struct
  add_disk(rb_dev.rb_disk);
  return 0;
}


static void sample_cleanup(void){
  int major = rb_dev.rb_disk->major;
	del_gendisk(rb_dev.rb_disk);
  put_disk(rb_dev.rb_disk);
  blk_cleanup_queue(rb_dev.rb_queue);
  unregister_blkdev(major, BLOCK_NAME);
	kfree(device_data);
	kfree(snapshot_data);
  printk(KERN_ALERT "DEBUG: End of cleanup\n");
  return;
}


static void rb_request(struct request_queue * rb_queue){
	int status;
	struct request *req;
	status = 0;
	while((req = blk_fetch_request(rb_queue))!=NULL){
		status = rb_transfert(req);
		if(status<0){
  		printk(KERN_ALERT "ERROR: rb_request rb_transfert\n");
			return;
		}
		__blk_end_request_all(req, status);
	}
}

static int rb_transfert(struct request *req){
	int rw, sec_cnt, nSectors, sector_offset;
	sector_t start_sec;
	struct req_iterator iter;
	void *buffer;
	struct bio_vec bv;

	rw = rq_data_dir(req);
	start_sec = blk_rq_pos(req);
	sec_cnt = blk_rq_sectors(req);
	sector_offset = 0;

	rq_for_each_segment(bv, req, iter){
		buffer = page_address(bv.bv_page) + bv.bv_offset;
		if((bv.bv_len % SECT_SIZE) != 0){
  		printk(KERN_ALERT "ERROR: rb_transfert bad buffer size\n");
			return -EIO;
		}
		nSectors = bv.bv_len / SECT_SIZE;
		if(rw == WRITE){
			memcpy(device_data+(start_sec+sector_offset)*SECT_SIZE, buffer, nSectors * SECT_SIZE);
		} else {
			memcpy(buffer, device_data+(start_sec+sector_offset)*SECT_SIZE ,nSectors * SECT_SIZE);
		}
		sector_offset += nSectors;
	}
	if(sector_offset != sec_cnt){
  	printk(KERN_ALERT "ERROR: bad sector offset sum nSectors = %d, sec_cnt = %d \n", sector_offset, sec_cnt);
		return -EIO;
	}
	return 0;
}

int get_password_size(char *pass){
	int pass_size = 0;
	while((pass_size < MAX_KEY_SIZE) && (pass[pass_size]!=0))
		pass_size ++;
	printk(KERN_ALERT "DEBUG: Password size = %d \n", pass_size);
	return pass_size;
}

int encrypt_decrypt_data(char *pass){
	int offset = 0;
	int pass_size = get_password_size(pass);

	while(offset < SECT_SIZE * NB_SECT){
		((char *)device_data)[offset] ^= pass[offset%pass_size];
		offset+=1;
	}
	return 0;
}

int ioctl_create_snapshot(void){
	memcpy(snapshot_data[nb_snapshot], device_data, NB_SECT * SECT_SIZE);
	if(current_snapshot==nb_snapshot-1){ // Never read snapshot beafore
		current_snapshot = nb_snapshot;
	}
	nb_snapshot +=1;
	return 0;
}


int ioctl_snapshot_before(void){
	if(current_snapshot < 0){
		printk(KERN_ALERT "ERROR: No snapshot registered");
		return -1;
	}
	memcpy(device_data, snapshot_data[current_snapshot], NB_SECT * SECT_SIZE);
	current_snapshot -= 1;
	return 0;
}


int ioctl_snapshot_after(void){
	if(current_snapshot == nb_snapshot -1){
		printk(KERN_ALERT "ERROR: No snapshot after");
		return -1;
	}
	memcpy(device_data, snapshot_data[current_snapshot], NB_SECT * SECT_SIZE);
	current_snapshot += 1;
	return 0;
}

static int ioctl_manager
(struct block_device *blkdev, fmode_t mode, unsigned int cmd, unsigned long arg)
{
	// Check command
	int status;
	if (_IOC_TYPE(cmd) == IOCTL_CREATE_SNAPSHOT_MAGIC)
		status = ioctl_create_snapshot();
	else if (_IOC_TYPE(cmd) == IOCTL_SNAPSHOT_BEFORE_MAGIC)
		status = ioctl_snapshot_before();
	else if (_IOC_TYPE(cmd) == IOCTL_SNAPSHOT_AFTER_MAGIC)
		status = ioctl_snapshot_after();
	else return -ENOTTY;
	if(status < 0){
		printk(KERN_ALERT "ERROR: During IOCTL \n");
		return(-1);
	}
	return 0;
}


static int rb_open(struct block_device *blk_dev, fmode_t mode){
//TODO
return 0;
}


static void rb_release(struct gendisk *gendsk, fmode_t mode){
// TODO
return ;
}


module_exit(sample_cleanup);
module_init(sample_init);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexis Cornard");
MODULE_DESCRIPTION("First block device");
MODULE_SUPPORTED_DEVICE("MyDevice");

